from unittest import TestCase
from calculator import Calculator

class TestCalculator(TestCase):
    def setUp(self):
        self.calculator = Calculator()

    def test_sum(self):
        """Docstring."""
        self.assertEqual(self.calculator.sum(1, 2), 3)
        self.assertEqual(self.calculator.sum(5, 6), 11)
        self.assertEqual(self.calculator.sum(5, 5), 10)
        self.assertEqual(self.calculator.sum(2, 12), 14)

    def test_multiply(self):
        """Docstring."""
        self.assertEqual(self.calculator.multiply(2, 2), 4)
        self.assertEqual(self.calculator.multiply(1, 2), 2)
        self.assertEqual(self.calculator.multiply(5, 4), 20)
        self.assertEqual(self.calculator.multiply(3, 12), 36)

    def test_subtract(self):
        """Docstring."""
        self.assertEqual(self.calculator.subtract(4, 2), 2)
        self.assertEqual(self.calculator.subtract(1, 3), -2)
        self.assertEqual(self.calculator.subtract(44, 2), 42)
        self.assertEqual(self.calculator.subtract(4, 1), 3)


    def test_divide(self):
        """Docstring."""
        self.assertEqual(self.calculator.divide(4, 1), 4)
        self.assertEqual(self.calculator.divide(4, 4), 1)
        self.assertEqual(self.calculator.divide(5, 1), 5)
        self.assertEqual(self.calculator.divide(4, 2), 2)



    def test_pi(self):
        """Docstring."""
        self.assertAlmostEqual(self.calculator.pi(), 3.14159)


if __name__ == "__main__":
    unittest.main()